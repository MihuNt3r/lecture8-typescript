export function fight(firstFighter, secondFighter) {
  var firstAttack = Math.floor(Math.random() * 2);

  var winner;
  if (firstAttack == 0){
    while (true){
      var damage = getDamage(firstFighter, secondFighter);

      secondFighter.health = secondFighter.health - damage;
      if (secondFighter.health <= 0){
        winner = firstFighter;
        break;
      }

      damage = getDamage(secondFighter, firstFighter);

      firstFighter.health = firstFighter.health - damage;
      if (firstFighter.health <= 0){
        winner = secondFighter;
        break;
      }
    }
  } else {
    while (true){
      var damage = getDamage(secondFighter, firstFighter);

      firstFighter.health = firstFighter.health - damage;
      if (firstFighter.health <= 0){
        winner = secondFighter;
        break;
      }

      damage = getDamage(firstFighter, secondFighter);

      secondFighter.health = secondFighter.health - damage;
      if (secondFighter.health <= 0){
        winner = firstFighter;
        break;
      }
    }
  }

  return winner;
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage 
  var hitPower = getHitPower(attacker);
  var blockPower = getBlockPower(enemy);
  var damage = hitPower - blockPower;
  if (damage < 0)
    damage = 0;
    
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  var criticalHitChance = Math.floor(Math.random() * 2) + 1;
  var hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  var dodgeChange = Math.floor(Math.random() * 2) + 1;
  var blockPower = fighter.defense * dodgeChange;
  return blockPower;
}
