import { showModal } from './modal';
import { createFighterDetails } from './fighterDetails';

export  function showWinnerModal(fighter) {
  // show winner name and image
  const title = 'Winner info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}