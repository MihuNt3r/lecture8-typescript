import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  console.log(fighter);
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterImage = createElement({ tagName: 'img', className: 'fighter-image' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health'});
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack'});
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense'});


  fighterImage.src = source;
  nameElement.innerText = '\nName: ' + name;
  healthElement.innerText = '\nHealth: ' + health;
  attackElement.innerText = '\nAttack: ' + attack;
  defenseElement.innerText ='\nDefence: ' + defense;
  fighterDetails.append(fighterImage);
  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);

  return fighterDetails;
}
